import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
disabled:true
mobileno:any
otp:any
visible:false
password:any
  constructor(private http:HttpClient) { }

  ngOnInit() {
  }
 
  getOtpData(event){
    var num =  this.mobileno;
    alert('OTP Has Been Texted To Your Mobile')
    this.disabled=true
    console.log(num)
  }
  verifyOTP(){
    const base_url='http://localhost:9005/verifyOtp/' +this.mobileno+'/'+this.otp+'/bhushan'
    this.http.post(base_url, {
    }).subscribe((data) => {
      console.log(base_url)
      if(data['status']==0){
        alert('The OTP has been Verified')
        this.visible=false
        this.disabled=true
      }else{
        alert('Please Try Again its Wrong OTP')
        window.location.reload()
      }
    })
  }
  registerData(event) {
    const base_url='http://localhost:9005/updateOrSaveData'
    this.http.post(base_url, {
      mobileno:this.mobileno,
      password:this.password
     
    }).subscribe((data) => {
      console.log(base_url)
      if(data['mobileno']==this.mobileno){
        alert('You are Sucessfully Added')
        window.location.href="/"
      }else{
        alert('Please Enter the Right Credentials')
        window.location.reload()
      }
    })
  }




}
