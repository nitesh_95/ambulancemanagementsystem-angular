import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-patientdata',
  templateUrl: './patientdata.component.html',
  styleUrls: ['./patientdata.component.scss']
})
export class PatientdataComponent implements OnInit {
  disabled: true
  name: any
  address: any
  emailid: any
  usertype: any
  locations: [];



  constructor(private http: HttpClient) { }

  ngOnInit() {
  }
  registerData(event) {
    console.log(this.name)
    const base_url = 'http://localhost:9005/updateOrSavePatientData'
    this.http.post(base_url, {
      patientname: this.name,
      address: this.address,
      emailid: this.emailid,
      usertype: "driver",
      latitude: 33.4,
      longitude: 32.3

    }).subscribe((data) => {
      console.log(this.name)
      console.log(base_url)
      if (data['patientname'] == this.name) {
        alert('Patient Details are Sucessfully Added')
        window.location.href='/home'
      } else {
        alert('Something Went Wrong')
        window.location.reload()
      }
    })
  }
}
