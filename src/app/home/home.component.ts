import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { ApiService } from '../core/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  disabled: true
  name: any
  address: any
  emailid: any
  usertype: any
  locations: [];

  searchCarForm = this.fb.group({
    location: new FormControl(null, Validators.required),
    start_date: new FormControl(new Date().toISOString().substring(0, 10), Validators.required)
  });

  constructor(private router: Router, private fb: FormBuilder, private api: ApiService, private http: HttpClient) { }

  ngOnInit() {
    this.api.getLocations().subscribe(res => this.locations = res);
  }
  registerData(event) {
    console.log(this.name)
    const base_url = 'http://localhost:9005/updateOrSavePatientData'
    this.http.post(base_url, {
      patientname: this.name,
      address: this.address,
      emailid: this.emailid,
      usertype: "driver",
      latitude: 33.4,
      longitude: 32.3

    }).subscribe((data) => {
      console.log(this.name)
      console.log(base_url)
      if (data['patientname'] == this.name) {
        alert('Patient Details are Sucessfully Added')
        window.location.href='/patientdetails'
      } else {
        alert('Something Went Wrong')
        window.location.reload()
      }
    })
  }
  onSubmit(form) {
    if (this.searchCarForm.valid) {
      const path = [
        'details',
        this.searchCarForm.controls.location.value,
        this.searchCarForm.controls.start_date.value
      ];

      this.router.navigate(path);
    }
  }

}
