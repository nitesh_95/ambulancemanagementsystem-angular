import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';

import { ApiService } from './core/api.service';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { DetailsComponent } from './details/details.component';
import { WeekStatusComponent } from './weekstatus/weekstatus.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { PatientdataComponent } from './patientdata/patientdata.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    DetailsComponent,
    WeekStatusComponent,
    FooterComponent,
    LoginComponent,
    RegistrationComponent,
    PatientdataComponent
  ],

  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,

    // Routes
    RouterModule.forRoot([
      { // /
        path: '',
        component: LoginComponent,
      },
      { // home
        path: 'registration',
        component: RegistrationComponent
      },
      { // home
        path: 'home',
        component: HomeComponent
      },
      { // home
        path: 'home',
        component: HomeComponent
      },
      { // home
        path: 'patientdetails',
        component: PatientdataComponent
      },
      { // details
        path: 'details',
        component: DetailsComponent
      },
      { // details with params
        path: 'details/:location/:start_date',
        component: DetailsComponent
      }
    ], { useHash: false })
  ],

  providers: [
    ApiService
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
