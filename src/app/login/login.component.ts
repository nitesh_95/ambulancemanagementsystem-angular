import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { url } from 'inspector';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  mobileno:any
  password:any

  constructor(private http:HttpClient) { }

  ngOnInit() {
  }
  getData(event) {
    const base_url='http://localhost:9005/verifyCredentials/'+this.mobileno+"/"+this.password
    console.log(this.password)
    this.http.post(base_url, {
    }).subscribe((data) => {
      console.log(base_url)
      if(data['status']==0){
        alert('The Credentials has been verified')
        window.location.href='/patientdetails'
        
      }else{
        alert('Please Enter the Right Credentials')
        window.location.reload()
      }
    })
  }

}
